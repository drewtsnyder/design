# Design for Drewtsnyder.com
For now this will serve as a place for me to put a list of resources that I'm using and maintain working notes.

## Resources
- [Foundation for Sites](http://foundation.zurb.com/sites)
- [Materialize](http://materializecss.com/) `Installed via Bower for post cards`
- [Foundation Style Sherpa](http://foundation.zurb.com/sites/docs/style-sherpa.html)
- [imDone Atom Task Manager](https://atom.io/packages/imdone-atom)

## Notes
-
